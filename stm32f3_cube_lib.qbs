import qbs

Project{
    property string MCU    : "STM32F334x8"
    property string board  : "stm32F4discovery"
    qbsSearchPaths         : ["~/projects/qbs_custom_modules/","."]
    references:["lib/lib.qbs"]
    property stringList cubeF3searchPaths:["~/projects/cpplibs/mcu/stm32/STM32Cube_FW_F3_V1.3.0",
        "~/projects/cpplibs/mcu/stm32/STM32Cube_FW_F3_V1.4.0"
    ]
    Product{
        Group{
            name:"module"
            files:[
                "modules/stm32f3cubelib/STM32F334x8/module.qbs",
                "modules/stm32f3cubelib/stm32f3cubelib.qbs",
            ]
        }
    }
}
