import qbs
import qbs.Probes

StaticLibrary{
    Depends{name:"cpp"}
    targetName:{
        var name_base = "stm32f3cube_"
        name_base = name_base + project.MCU
        if(qbs.buildVariant == "debug")
            name_base = name_base + "d"

        return name_base
    }

    property string __home:{
        var home
        if(qbs.hostOS.contains("linux"))
            home = qbs.getEnv("HOME")
        else
            home = "d:"
        return home
    }
    property stringList searchPaths:project.cubeF3searchPaths
    property stringList __searchPaths:{
        var res = [];
        for (var i = 0; i < searchPaths.length; ++i){
            var s = ""
            if (searchPaths[i].startsWith("~")){
                s = searchPaths[i].replace("~", __home)
            }
            else{
                s = searchPaths[i]
            }
            res.push(s)
        }
        return res
    }

    Probes.PathProbe{
        id:path_of_stm32f3cube
        names:["stm32f3xx.h"]
        pathSuffixes:["", "Drivers/CMSIS/Device/ST/STM32F3xx/Include"]
        platformPaths:{
            var res = []
            res = res.concat(__searchPaths)
            return res
        }
    }
    property string periph_lib_path:{
        var pp = "/NO_WAY"
        if(path_of_stm32f3cube.found){
            pp = path_of_stm32f3cube.path
            var ccc = pp.split("/")
            var rrr = []
            if(ccc.length > 6){
                for (var i = 0; i < ccc.length - 6; ++i){
                    rrr.push(ccc[i])
                }
                pp = rrr.join("/")
            }
        }
        return pp
    }



    Group {
        name: "Product"
        fileTagsFilter: product.type
        qbs.install: true
        qbs.installDir: "lib"
    }

    Group{
        name:"config"
        files:[
            "../config/stm32f3xx_hal_conf.h",
        ]
    }

    Group{
        name:"CMSIS"
        prefix: {
            var pref = periph_lib_path + "/Drivers/CMSIS/Include/"
            return pref
        }
        files:[
            "arm_common_tables.h",
            "arm_const_structs.h",
            "arm_math.h",
            "core_cm4.h",
            "core_cmFunc.h",
            "core_cmInstr.h",
        ]
    }
    Group{
        name:"device"
        prefix: {
            var pref = periph_lib_path + "/Drivers/CMSIS/Device/ST/STM32F3xx/Include/"
            return pref
        }
        files:["*.h"]
    }
    Group{
        name:"math"
        prefix: {
            var pref = periph_lib_path + "/Drivers/CMSIS/DSP_Lib/Source/"
            return pref
        }
        files:[
            "BasicMathFunctions/*.c",
            "CommonTables/*.c",
            "ComplexMathFunctions/*.c",
            "ControllerFunctions/*.c",
            "FastMathFunctions/*.c",
            "FilteringFunctions/*.c",
            "MatrixFunctions/*.c",
            "StatisticsFunctions/*.c",
            "SupportFunctions/*.c",
            "TransformFunctions/*.c"
        ]
    }
    Group{
        name:"periph"
        prefix: {
            var pref = periph_lib_path + "/Drivers/STM32F3xx_HAL_Driver/"
            return pref
        }
        files:{
            var files = ["Inc/*.h"]
            files = files.concat(["Src/*.c"])
            return files
        }
    }


    cpp.defines:{
        var defs = base
        defs = defs.concat(["USE_HAL_DRIVER","USE_FULL_ASSERT","ARM_MATH_CM4"])
        defs = defs.concat([project.MCU, "__FPU_PRESENT=1"])
        return defs
    }

    cpp.includePaths:{
         var paths = base
         paths = paths.concat([periph_lib_path + "/Drivers/CMSIS/Include/",
                               periph_lib_path + "/Drivers/STM32F3xx_HAL_Driver/Inc",
                               periph_lib_path + "/Drivers/CMSIS/Device/ST/STM32F3xx/Include/",
                               "../config",
                              ])
         return paths
     }

    cpp.commonCompilerFlags: ["-fdata-sections","-ffunction-sections"]

    property stringList MCU_FLAGS: ["-mthumb","-mcpu=cortex-m4"]
    property stringList FPU_FLAGS: ["-mfloat-abi=hard", "-mfpu=fpv4-sp-d16" ]

    cpp.cxxFlags:{
        var flags = base
        flags = flags.concat(["-std=gnu++14","-fno-exceptions","-fno-rtti"])
        flags = flags.concat(MCU_FLAGS)
        flags = flags.concat(FPU_FLAGS)
        return flags
    }

    cpp.cFlags:{
        var flags = base.concat(["-std=gnu11"])
        flags = flags.concat(MCU_FLAGS)
        flags = flags.concat(FPU_FLAGS)
        return flags
    }

    cpp.linkerFlags:{
        var flags = base
        flags = flags.concat(MCU_FLAGS)
        flags = flags.concat(FPU_FLAGS)
        flags = flags.concat(["-specs=nano.specs","-specs=rdimon.specs"])
        return flags
    }
}
